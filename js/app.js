(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {
        getForecast('mumbai')
            .then(data=>console.upateUI(data,'Mumbai'))
            .catch(err=>console.log(err));
    });

})(jQuery, document, window);


/*****************************************************
**************COMMUNICATING WITH FORECAST.JS**********
******************************************************/

const defaultCity = "Mumbai";
const cityForm = document.querySelector('form');
const cityTextField = document.getElementById('city');

const daysName = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const monthsName = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
    
function updateUI(data,cityName){
    const days = document.getElementsByClassName('day');
    const date = document.getElementsByClassName('date');
    const location = document.getElementsByClassName('location');
    const temps = document.getElementsByClassName('temp');
    const icons = document.getElementsByClassName('weather-icon');
    const humidity = document.getElementById('humidity');
    const windSpeed = document.getElementById('wind-speed');
    const windDegree = document.getElementById('wind-degree');
    
    location[0].innerHTML = cityName;
    humidity.innerHTML = data.list[0].main.humidity +'%';
    //1 m/sec = 3.6km/hr
    windSpeed.innerHTML = Math.round((data.list[0].wind.speed * 3.6 * 10))/10 + "km/hr";
    windDegree.innerHTML = data.list[0].wind.deg + "<sup>0</sup>";
    
    const todaysDate = new Date(data.list[0].dt_txt);
    const todaysMonth = monthsName[todaysDate.getMonth()];
    const todaysDay = todaysDate.getDay();
    date[0].innerHTML = todaysDate.getDate() + " " + todaysMonth;
    const imagePath = 'images/icons/';
    var i = 0, j=0;
    for(let element of days){
        const dayName = daysName[(todaysDay + i) %7];
        element.innerHTML = dayName;
        
        const temp = Math.round(data.list[j].main.temp *10)/10;
        temps[i].innerHTML = temp + '<sup>o</sup>';
        icons[i].src = imagePath + data.list[j].weather[0].icon + ".svg";
        i++;
        j+=8;
    }
}
cityForm.addEventListener('submit',e =>{
    e.preventDefault();
    let cityName =cityTextField.value;
    if(cityName == ""){
       cityName = defaultCity;
    }
    
    getForecast(cityName)
        .then(data => updateUI(data,cityName))
        .catch(err => console.error(err));
});