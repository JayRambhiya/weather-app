//http://api.openweathermap.org/data/2.5/forecast?q=mumbai&units=metric&APPID=f2c61246c2d2e24b4db798ac4bf90430

const key  = "f2c61246c2d2e24b4db798ac4bf90430";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&APPID=${key}`;
    
    const response = await fetch(base+query);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error status:'+response.status);
    }
}
//getForecast()
//    .then(data=>console.log(data))
//    .catch(err=>console.log(err));